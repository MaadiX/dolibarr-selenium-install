Dolibarr selenium Install
=========================

Script to carry out dolibarr wizard installation as it does not provide any CLI for this purpose.

NoSuchElementException is used so we can recover process in case it was interrupted at some point before terminating.

The script requires the url of the dolibarr installation an the desired user passowrd as parameter:

eg:

```
wizard-install.py https://my.dolibarr.com Mywonderfulpassword

```

If you need to set chrome/chromium language:
```
LANGUAGE=en wizard-install.py https://my.dolibarr.com Mywonderfulpassword

```

