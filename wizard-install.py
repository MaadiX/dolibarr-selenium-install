#!/usr/bin/python3

import os, sys
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.common.exceptions import NoSuchElementException

if __name__ == "__main__":

    if len(sys.argv) !=3:
        print("Usage: You must include a valid url and a password")
        sys.exit(1)
    url= sys.argv[1]
    user_password = sys.argv[2]

    try:
        options = ChromeOptions()
        options.add_argument('--headless')
        # Sandox is needed for launching script as root. We actually do not need it
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-infobars")
        options.add_argument("--disable-extensions")
        options.add_argument("--disable-dev-shm-usage")
        options.add_argument("--remote-debugging-port=9222")
        driver = webdriver.Chrome(options=options)

    except Exception as e:
        print("Could not create diver:", e)
        sys.exit(1)

    driver.get(url)

    # If installation has to be done we will be redierected to
    # the install url
    if "install" not in driver.current_url:
        print("Installation already completed. Can't do anything")
        sys.exit(2)

    title = driver.title
    #TODO: assert by title may be dangerous as it changes accorging to language.
    #assert title == "Instalación/Actualización de Dolibarr"
    print("title", title)
    driver.implicitly_wait(0.5)

    # First Step- Just clic submit ad set language
    try:
        #Uncomment if you need to set default language
        #text_box = driver.find_element(by=By.ID, value="selectlang")
        #text_box.send_keys("es_ES")
        submit_button = driver.find_element(by=By.CSS_SELECTOR, value="input[type='submit']")
        submit_button.click()
        print("Now on: ", driver.current_url)
    except NoSuchElementException:
        pass

    #Second Step
    try:
        button2 = driver.find_element(by=By.CLASS_NAME, value="button")
        button2.click()
        print("Now on: ", driver.current_url)
    except NoSuchElementException:
        pass

    try:
        submit_button = driver.find_element(by=By.CSS_SELECTOR, value="input[type='submit']")
        submit_button.click()
        print("Now on: ", driver.current_url)
    except NoSuchElementException:
        pass

    # Nou url changes
    if 'step1.php' in driver.current_url:
        try:
            # If the submit button does not exists, there was an error in previous step
            # Need to hable it
            submit_button = driver.find_element(by=By.CSS_SELECTOR, value="input[type='submit']")
            submit_button.click()
            print("Now on: ", driver.current_url)
        except NoSuchElementException:
            pass

    if 'action=upgrade' in driver.current_url or 'step2.php' in driver.current_url:
        try:
            submit_button = driver.find_element(by=By.CSS_SELECTOR, value="input[type='submit']")
            submit_button.click()
            print("Now on: ", driver.current_url)
        except NoSuchElementException:
            pass
    if 'step4.php' in driver.current_url:
        try:
            password = driver.find_element(by=By.ID, value="pass")
            password.send_keys(user_password)
            password2 = driver.find_element(by=By.ID, value="pass_verif")
            password2.send_keys(user_password)
            submit_button = driver.find_element(by=By.CSS_SELECTOR, value="input[type='submit']")
            submit_button.click()
            print("Now on: ", driver.current_url)
        except NoSuchElementException:
            pass
    if 'step5.php' in driver.current_url:
        # It seems this is the last step
        # The message returned by dolibarr is not very clear
        print("Upgrade completed")

    driver.quit()
    sys.exit(os.EX_OK)

